<?php
    $imagepath = __DIR__."image.png";           //путь к файлу
    $image = imagecreatefrompng($imagepath);    //создаем изображение из файла
    $wImage = 20000;
    $hImage = 20000;
    $wBanner = 200;
    $hBanner = 100;
    
    $banner = imagecreatetruecolor($wBanner, $hBanner);             //Создаем баннер 200х100
    $sidecollor = imagecolorallocatealpha($banner, 0, 0, 0, 127);   //Цвет фона баннера
    imagefill($banner, 0, 0, $sidecollor);                          //Заливка
    imagecopyresampled($banner, $image, 50, 0, 0, 0, 100, 100, $wImage, $hImage); //Помещаем изображение на баннер
    
    imagepng($banner);  //Выводим изображение
?>