<?php
//Массив может быть ассоциативным (индекс вместо целого числа может быть строковым ключом), 
//поэтому лучше использовать цикл foreach вместо прямой проверки по индексам циклом for
function countpairs ($mass) {
    $previos;
    $count = 0;
    foreach ($mass as $current) {
        if ((isset($previos)) && ($current === $previos)) { //если previos не установлено, то это первая итерация, и сравнение пропускаем
            $count++;
        }
        $previos = $current;
    }
    echo $count;
}
?>